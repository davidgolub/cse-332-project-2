package testA;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import phaseA.AVLTree;
import providedCode.Comparator;
import providedCode.DataCount;
import providedCode.DataCounter;
import providedCode.SimpleIterator;
import test.TestDataCounter;


public class TestMoveToFrontList extends TestDataCounter<String> {


private static final int TIMEOUT = 2000000; // 2000ms = 2sec
	
	/** Creates AVLTree before each test cases **/
	@Override
	public DataCounter<String> getDataCounter() {
		return new AVLTree<String>(new Comparator<String>() {
			public int compare(String e1, String e2) { return e1.compareTo(e2); }
		});
	}
	
	/** Creates AVLTree before each test cases **/
	public DataCounter<Integer> getIntCounter() {
		return new AVLTree<Integer>(new Comparator<Integer>() {
			public int compare(Integer e1, Integer e2) { return e1 - e2; }
		});
	}

	/** Test Size ===========================================================**/

	@Test(timeout = TIMEOUT)
	public void test_size_empty(){
		assertEquals("Tree should have size 0 when constructed", 0, dc.getSize());
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_size_after_adding_single_num(){
		addAndTestSize("Tree should have size 1 after adding single 5", new int[]{5}, 1);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_insert_right_right_subtree(){
		int[] testArray = {0,1,2};
		addAndTestRoot("Tree should root 1 after adding 0, 1, 2", testArray, 1);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_insert_left_left_subtree(){
		int[] testArray = {2,1,0};
		addAndTestRoot("Tree should root 1 after adding 2, 1, 0", testArray, 1);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_insert_right_left_subtree(){
		int[] testArray = {1,3,2};
		addAndTestRoot("Tree should root 2 after adding 1, 3, 2", testArray, 2);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_insert_left_right_subtree_null(){
		int[] testArray = {3,1,2};
		addAndTestRoot("Tree should root 2 after adding 3, 1, 2", testArray, 2);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_insert_left_right_subtree_not_root(){
		int[] testArray = {1,0,4,5,2,3};
		addAndTestRoot("Tree should root 2 after adding 1,0,4,5,2,3", testArray, 2);
	}
	
	@Test(timeout = TIMEOUT) 
	public void test_left_insert_right_right_subtree_not_root(){
		int[] testArray = {1,0,4,6,2,5};
		addAndTestRoot("Tree should root 4 after adding 1,0,4,5,2,3", testArray, 4);
	}
	
	@Test(timeout = TIMEOUT)
	public void test_size_after_adding_many_same_num(){
		addAndTestSize("Tree should have size 1 after adding multiple 5", new int[]{5,5,5}, 1);
	}

	@Test(timeout = TIMEOUT)
	public void test_size_after_adding_unique_nums(){
		int[] testArray = {0,1,2,3,4};
		addAndTestSize("Added " + Arrays.toString(testArray), testArray, 5);
	}
	
	@Test(timeout = TIMEOUT)
	public void test_size_after_adding_duplicate_nums(){
		int[] testArray = {0,0,1,1,2,2,3,3,4,4};
		addAndTestSize("Added " + Arrays.toString(testArray), testArray, 5);
	}
	
	
	
	/** Test getCount =======================================================**/
	
	@Test(timeout = TIMEOUT)
	public void test_get_count_after_adding_many_same_num(){
		int key = 9;
		int[] testArray = {9, 9, 9, 9, 9, 9, 9};
		addAndGetCount("Added " + Arrays.toString(testArray) + ", key=" + key, testArray, key, 7);
	}
	
	@Test(timeout = TIMEOUT)
	public void test_get_count_after_adding_many_diff_nums(){
		int[] testArray = {0, 5, 3, 4, 20, 15, 6, 32, 64, 182, 162, -1, 7, 1, 10, 2};
		for (int i = 0; i < testArray.length; i++) {
			int key = testArray[i];
			addAndGetCount("Added " + Arrays.toString(testArray) + ", key=" + key, testArray, key, i + 1);
		}
	}
	
	
	
	/** Test Iterator =======================================================**/

	@Test(timeout = TIMEOUT, expected = java.util.NoSuchElementException.class)
	public void test_iterator_empty() {
		SimpleIterator<DataCount<String>> iter = dc.getIterator();
		iter.next(); 
	}
	
	@Test(timeout = TIMEOUT)
	public void test_iterator_get_all_data() {

	    int[] startArray = {7, 5, 5, 6, 6, 1, 9, 4, 8, 6};

	    // Expected array has no duplicates and is sorted
	    for (int i = 0; i < startArray.length; i++) { 
		dc.incCount("" + startArray[i]); 
	    }

	    String[] expected = { "1", "4", "5", "6", "7", "8", "9" };

	    // Actual array returned by the iterator
	    int i = 0;
	    SimpleIterator<DataCount<String>> iter = dc.getIterator();
	    String[] actual = new String[expected.length];
	    while (iter.hasNext()) { 
		actual[i++] = iter.next().data; 
	    }

	    // Sort and test
	    Arrays.sort(actual);
	    assertArrayEquals("Added " + Arrays.toString(startArray) + 
			      "Got " + Arrays.toString(actual), expected, actual);

	}	
	
	
	/** Private methods =====================================================**/

	private void addAndTestSize(String message, int[] input, int unique) {
		for (int num : input) { 
		    dc.incCount("" + num); 
		}
		assertEquals(message, unique, dc.getSize());
	}
	
	/** Private methods =====================================================**/

	private void addAndTestRoot(String message, int[] input, int rootVal) {
		for (int num : input) { 
		    dc.incCount("" + num); 
		}
		String rootData = dc.getIterator().next().data;
		assertEquals(message, "" + rootVal, rootData);
	}
	
	private void addAndGetCount(String message, int[] input, int key, int expected) {
		for (int num : input) { 
		    dc.incCount("" + num); 
		}
		assertEquals(message, expected, dc.getCount("" + key));
	}

}
