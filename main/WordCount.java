package main;
import java.io.IOException;
import java.lang.NullPointerException;
import phaseA.*;
import providedCode.*;

/**
 * An executable that counts the words in a files and prints out the counts in
 * descending order. You will need to modify this file.
 */
public class WordCount {

	
	// TODO: Replace this comment with your own as appropriate.
	// You may modify this method if you want.
    private static void countWords(String file, DataCounter<String> counter) {
        try {
            FileWordReader reader = new FileWordReader(file);
            String word = reader.nextWord();
            while (word != null) {
                counter.incCount(word);
                word = reader.nextWord();
            }
        }catch (IOException e) {
            System.err.println("Error processing " + file + " " + e);
            System.exit(1);
        }
    }
    
    
    /** 
     * Counts the number of words in a file and prints out 
     * word, count pairs in new lines
     * @throws NullPointerException if s1 or s2 are null
 	 */
	@SuppressWarnings("unchecked")
 	private static <E> DataCount<E>[] getCountsArray(DataCounter<E> counter) {
		if (counter == null) {
			throw new NullPointerException();
		}
		// Init the array to store all of the elements
 		int numElements = counter.getSize();
		DataCount<E>[] wordCounts = (DataCount<E>[]) new DataCount[numElements];
		
		// Iterate through the word counts and add items into array
 		SimpleIterator<DataCount<E>> iterator = counter.getIterator();
 		int currIndex = 0;
 		while (iterator.hasNext()) {
 			wordCounts[currIndex] = iterator.next();
 			currIndex++;
 		}
 		
 		return wordCounts;
 	}
    
 	
    // IMPORTANT: Used for grading. Do not modify the printing *format*!
    // Otherwise you may modify this method (its signature, or internals) 
    // if you want.
    private static void printDataCount(DataCount<String>[] counts) {
    	for (DataCount<String> c : counts) {
            System.out.println(c.count + "\t" + c.data);
        }
    }
    
    
    /** 
     * Counts the number of words in a file and prints out 
     * word, count pairs in new lines
     * @Requires valid filename 
 	 */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: filename of document to analyze");
            System.exit(1);
        }
        DataCounter<String> counter = new AVLTree<String>(new StringComparator());
        countWords(args[0], counter); 
        DataCount<String>[] counts = getCountsArray(counter);
        Sorter.insertionSort(counts, new DataCountStringComparator());
        printDataCount(counts);
    }
}
