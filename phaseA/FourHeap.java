package phaseA;
import java.util.NoSuchElementException;

import providedCode.*;


/**
 * FourHeap implements the DataCounter interface using a
 * min four-heap.
 * The constructor takes a Comparator<? super E> "function object" so
 * that items of type E can be compared.
 * Each heap node associates a count with an E.
 */
public class FourHeap<E> extends Heap<E> {
	private int capacity;  // capacity of heap
	private int size;
	private Comparator<? super E> comparator;
	
	public FourHeap(Comparator<? super E> c) {
		super(10);
		this.capacity = 10;
		this.size = 0;
		this.comparator = c;
	}
	
	/*
	 * Doubles the size of internal array storing data
	 * */
	@SuppressWarnings("unchecked")
	private void doubleCapacity() {
		// create new array
		int newCapacity = this.capacity * 2;
		E[] newArr = (E[]) new Object[newCapacity];
		
		// copy all elements into this array
		for (int i = 0; i < this.size; i++) {
			newArr[i] = this.heapArray[i];
		}
		
		// make a pointer to this array, double capacity
		this.heapArray = newArr;
		this.capacity *= 2;
	}

	@Override
	public void insert(E item) {
		// double array if necessary
		if (this.size == this.capacity - 2) {
			this.doubleCapacity();
		}

		// percolate up
		int index = this.percolateUp(item, this.size);
		this.heapArray[index] = item;
		
		this.size++;
	}

	private int percolateUp(E item, int hole) {
		while (hole > 0 && this.comparator.compare(item, this.heapArray[(hole - 1) / 4]) < 0) {
			this.heapArray[hole] = this.heapArray[(hole - 1) / 4];
			hole = (hole - 1) / 4;
		}
		return hole;
	}
	
	private int percolateDown(E item, int hole) {
		while ((hole * 4 + 1) < size) {
			int childIndex = hole * 4 + 1;
			int minIndex = childIndex;
			E minVal = this.heapArray[minIndex];
			for (int i = 1; i < 4; i++) {
				E currVal = this.heapArray[childIndex + i];
				if (this.comparator.compare(currVal, minVal) < 0) {
					minIndex = childIndex + i;
					minVal = currVal;
				}
			}
			if (this.comparator.compare(minVal, item) < 0) {
				this.heapArray[hole] = this.heapArray[minIndex];
				hole = minIndex;
			} else break;
		}
		return hole;
	}

	/**
	 * Finds min element in heap and returns it.
	 * @throws NoSuchElementException if heap is empty
	 * */
	@Override
	public E findMin() {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}
		return this.heapArray[1];
	}

	/**
	 * Returns and deletes minimum element in heap
	 * @return min element in heap
	 * @throws NoSuchElementException if heap is empty
	 */
	@Override
	public E deleteMin() {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}
		
		E deletedVal = this.heapArray[0];
		
		int index = this.percolateDown(this.heapArray[size], 0);
		this.heapArray[index] = this.heapArray[size];
		this.size--;
		
		return deletedVal;
	}

	@Override
	public boolean isEmpty() {
		return size < 1;
	}

}
