package phaseA;
import providedCode.*;
import java.util.EmptyStackException;
/**
 * A class for a type-generic stack using an array.
 */
public class GArrayStack<T> implements GStack<T> {
	private T[] data; 
	private int frontIndex; 
	private int size; 
	
	/**
     * Creates a new instance of an empty stack
     */
	@SuppressWarnings("unchecked")
	public GArrayStack(){
		this.frontIndex = -1;
		this.size = 10;
		this.data = (T[]) new Object[size];
	}
	
	/**
	 * Returns true if the stack is empty, false otherwise
	*/
	public boolean isEmpty() {
		return this.frontIndex == -1;
	}

	/**
     * Pushes an element to the front of the stack
     */
	public void push(T val) {
		//set front pointer to this new value
		this.frontIndex++;
		this.data[this.frontIndex] = val;
		
		//increase the size of the stack if necessary
		if (this.frontIndex == this.size - 1) {
			this.increaseSize();
		}
	}
	
	/**
     * Increases the size of the stack
     */
	@SuppressWarnings("unchecked")
	private void increaseSize() {
		int prevSize = this.size;
		this.size *= 2;
		
		T[] newData = (T[]) new Object[this.size];
		
		for (int i = 0; i < prevSize; i++) {
			newData[i] = this.data[i];
		}
		
		this.data = newData;
	}

	/**
     * Removes and returns the element at front of the stack
     * @throws EmptyStackException if the stack is empty
     */
	public T pop() {
		if (this.isEmpty()) {
			throw new EmptyStackException();
		}
		
		T val = this.data[this.frontIndex];
		frontIndex--;
		
		return val;
	}

    /**
     * Returns the element at front of the stack without removing it
     * @throws EmptyStackException if the stack is empty
     */
	public T peek() {
		if (this.isEmpty()) {
			throw new EmptyStackException();
		}
		
		T val = this.data[this.frontIndex];
		return val;
	}

}