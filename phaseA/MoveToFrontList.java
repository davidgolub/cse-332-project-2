package phaseA;
import java.util.NoSuchElementException;

import providedCode.*;


/**
 * MoveToFrontList implements the DataCounter interface using an unsorted
 * linked list, where more frequent items appear earlier on. 
 */

public class MoveToFrontList<E> extends DataCounter<E> {
	private int size; 
	private Comparator<? super E> comparator;
	protected MoveToFrontList<E>.ListNode front;
	
	public MoveToFrontList(Comparator<? super E> c) {
		this.comparator = c;
		this.front = null;
		this.size = 0;
	}
	
	/*
	 * Increments the count of the data element in the linked list
	 * and moves element to the front. If element is not found,
	 * adds a new element (with count 1) to front of the list.
	 * @throws NullPointerException if data is null
	 * */
	@Override
	public void incCount(E data) {
		// check that input is valid
		if (data == null) {
			throw new NullPointerException();
		}
		
		// iterate through list to see if data is already there
		ListNode curr = this.front;
		
		// keep track of previous pointer to update the list in the end
		ListNode prev = null;
		
		while (curr != null) {
			int areEqual = this.comparator.compare(data, curr.dataCount.data);
			if (areEqual == 0) { 
				// found our element
				curr.dataCount.count += 1;
				
				// move it to front of list if it isn't already
				if (prev != null) {
					prev.next = curr.next;
					curr.next = this.front;
					this.front = curr;
				} 
				return;
			}
			
			// otherwise move on to next element
			prev = curr;
			curr = curr.next;
		}
		
		// Element was not found, so make a new one
		DataCount<E> dataCount = new DataCount<E>(data, 1);
		ListNode newNode = new ListNode(dataCount);
		this.size++;
		
		// Move this element to front
		newNode.next = this.front;
		this.front = newNode;
	}

	/**
	 * @return number of elements in the list
	 * */
	@Override
	public int getSize() {
		return this.size;
	}

	/**
	 * Returns the count of specified data element
	 * if element is not found, returns -1.
	 * @throws NullPointerException if data is null
	 * @return Number of times data element occured, -1 otherwise
	 * */
	@Override
	public int getCount(E data) {
		if (data == null) {
			throw new NullPointerException();
		}
		int count = 0;
		
		// keep track of previous pointer to update the list in the end
		ListNode curr = this.front;
		ListNode prev = null;
		
		// go through the list until reach element
		while (curr != null) {
			int areEqual = this.comparator.compare(data, curr.dataCount.data);
			if (areEqual == 0) {
				// get the count
				count = curr.dataCount.count;
				
				// put element to front of list
				if (prev != null) { //meaning element is not at front
					prev.next = curr.next;
					curr.next = this.front;
					this.front = curr;
				}
				break;
			}
			prev = curr;
			curr = curr.next;
		}
		
		return count;
	}

	/**
	 * @return An iterator over the list with two methods
	 * next() and hasNext()
	 */
	@Override
	public SimpleIterator<DataCount<E>> getIterator() {
		// TODO Auto-generated method stub
		return new SimpleIterator<DataCount<E>>() {
			ListNode curr = null;
			@Override
			public DataCount<E> next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				} 
					curr = curr == null ? front : curr.next;
					return curr.dataCount;
			}

			@Override
			public boolean hasNext() {
				// either current is null and front of list is not null
				// or current is non-null and next element is non null
				boolean nullCurr = curr == null && front != null;
				boolean regCurr = curr != null && curr.next != null;
	
				return nullCurr || regCurr;
			}
		};
	}
	
	/** Private Class ==================================================**/

    /**
     * Inner class to represent a node in the linked list. Each node includes
     * a data of type E, and an integer count. 
     */
	private class ListNode {
		private DataCount<E> dataCount;
		private ListNode next;
		
		public ListNode(DataCount<E> d) {
			this.dataCount = d;
			this.next = null;
		}
	}

}
