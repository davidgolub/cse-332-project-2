package phaseA;
import providedCode.*;
import java.lang.NullPointerException;

/**
 * TODO: REPLACE this comment with your own as appropriate.
 * 1. This comparator is used by the provided code for both
 *    data-counters and sorting.  Because of how the output must be
 *    sorted in the case of ties, your implementation should return a
 *    negative number when the first argument to compare comes first
 *    alphabetically.
 * 2. Do NOT use any String comparison provided in Java's standard
 *    library; the only String methods you should use are length and
 *    charAt.
 * 3. You can use ASCII character codes to easily compare characters
 *    http://www.asciitable.com/
 * 4. When you are unsure about the ordering, you can try
 *    str1.compareTo(str2) to see what happens.  Your
 *    stringComparator.compare(str1, str2) should behave the same way
 *    as str1.compareTo(str2).  They don't have to return the same
 *    value, but their return values should have the same sign (+,- or
 *    0).
 */
public class StringComparator implements Comparator<String>{

    /**
     * Returns -1 if first string comes alphabetically
     * before second string, 1 otherwise.
     * @throws NullPointerException if s1 or s2 are null
     */
	@Override
	public int compare(String s1, String s2) {
		int stringOrder = 0;
		// Check if strings are non-null
		if (s1 == null || s2 == null) {
			throw new NullPointerException();
		}
		
		// Get lengths of strings
		int firstLength = s1.length();
		int secondLength = s2.length();
		
		// Iterate through each character and check
		for (int i = 0; i < firstLength; i++) {
			if (i >= secondLength) { 
				stringOrder = 1;
				break;
			}
			// Get the characters from each string
			char firstChar = s1.charAt(i);
			char secondChar = s2.charAt(i);
			
			// See if characters don't match up
			if (firstChar < secondChar) {
				stringOrder = -1;
				break;
			} else if (firstChar > secondChar) {
				stringOrder = 1; 
				break;
			}
		}
		
		// If match up on all characters of first string, but second one is longer
		if (stringOrder == 0 && secondLength > firstLength) {
			stringOrder = -1;
		}
		return stringOrder;
	}
}
