package phaseA;

import providedCode.*;


/**
 * AVLSearchTree implements the DataCounter interface using a
 * an AVL tree.
 * The constructor takes a Comparator<? super E> "function object" so
 * that items of type E can be compared.
 * Each tree node associates a count with an E.
 */
public class AVLTree<E> extends BinarySearchTree<E> {

	/**
	 * Creates an empty AVL Tree
	 * @param comparator used to order elements
	 */
	public AVLTree(Comparator<? super E> comparator) {
		super(comparator);
	}
	
    /** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void incCount(E data) {
        if (overallRoot == null) {
            overallRoot = new AVLNode(data);
            this.size++;
            return;
        }
        
		AVLNode currentNode = (AVLNode)overallRoot;
		GStack<AVLNode> affectedNodes = new GArrayStack<AVLNode>();
		GStack<Boolean> leftOrRight = new GArrayStack<Boolean>();
		leftOrRight.push(false); 
		
		// push nodes in
        while (true) {
        	affectedNodes.push(currentNode);
        	boolean isEmpty = affectedNodes.isEmpty();
        	System.out.println(isEmpty);
        	// compare the new data with the data at the current node
        	int cmp = comparator.compare(data, currentNode.data);
        	if(cmp == 0) {            // a. Current node is a match
        		currentNode.count++;
        		break;
            }else if(cmp < 0) {       // b. Data goes left of current node
            	if(currentNode.left == null) {
            		// create a new node to store data
            		currentNode.left = new AVLNode(data);
            		
            		// push values for backtracking
            		leftOrRight.push(true);
            		affectedNodes.push((AVLNode)currentNode.left);
            		this.size++;
            		break;
                }	
            	leftOrRight.push(true);
            	currentNode = (AVLNode)currentNode.left;
            }else{                    // c. Data goes right of current node
            	if(currentNode.right == null) {
            		// create a new node to store data, push to stak
            		currentNode.right = new AVLNode(data);
            		
            		// push values for backtracking
            		leftOrRight.push(false);
            		affectedNodes.push((AVLNode)currentNode.right);
            		this.size++;
            		break;
                }
            	leftOrRight.push(false);
                currentNode = (AVLNode)currentNode.right;
            }
        }
        
        this.updateTree(affectedNodes, leftOrRight);
	}
	
	/**
	 * Update tree structure to restore balance property
	 * after inserting a node
	 * @param affectedNodes: nodes traversed while inserting current value
	 * @param leftOrRight: whether nodes were left or right of parent
	 */
	@SuppressWarnings("unchecked")
	private void updateTree(GStack<AVLNode> affectedNodes, GStack<Boolean> leftOrRight) {
		// pop inserted node off of the stack, don't actually need it
		AVLNode currNode = affectedNodes.pop();
		boolean prevIsLeft = false;
		
		// only need to check if tree is balanced while imbalance isn't seen
		boolean imbalanceSeen = false;
		boolean currIsLeft = leftOrRight.pop();
		
		// iterate through nodes and update heights
		while (!affectedNodes.isEmpty()) {
			currNode = affectedNodes.pop();
			
			// see if the tree is balanced
			if (!imbalanceSeen) {
				boolean isBalanced = this.isBalanced(currNode);
				if (!isBalanced) {
					imbalanceSeen = true;
					currNode = (AVLNode)this.rotateTree(prevIsLeft, currIsLeft, currNode);
					this.updateHeight(currNode);
					if (!affectedNodes.isEmpty()) {
						this.updateHeight(currNode);
						AVLNode nextNode = affectedNodes.pop();
						currIsLeft = leftOrRight.pop();
						if (currIsLeft) {
							nextNode.left = currNode;
						} else {
							nextNode.right = currNode;
						}
						currNode = nextNode;
					}
				} else {
					prevIsLeft = currIsLeft;
					currIsLeft = leftOrRight.pop();
				}
			}
			this.updateHeight(currNode);
		}
		this.overallRoot = currNode;
	}
	
	/**
	 * Rotates tree at specified node to restore balanced property.
	 * @param prevIsLeft: whether inserted node in left subtree of child
	 * @param currIsLeft: whether child is left/right of current node
	 * @param root: node where balance property was violated
	 * @return new rotated node with restored balance property
	 */
	private BSTNode rotateTree(boolean prevIsLeft, boolean currIsLeft, AVLNode root) {
		if (prevIsLeft && currIsLeft) {          // case 1
			return this.rotateWithLeft(root);
		} else if (!prevIsLeft && currIsLeft) {  // case 2
			return this.doubleRotateWithLeft(root);
		} else if (prevIsLeft && !currIsLeft) {  // case 3
			return this.doubleRotateWithRight(root);
		} else {  								 // case 4
			return this.rotateWithRight(root);
		}
	}
	
	/**
	 * Makes a right-right rotation on the root node
	 * @param root: node to rotate
	 * @return rotated node
	 */
	private BSTNode rotateWithRight(BSTNode root) {
		BSTNode temp = root.right;
		root.right = temp.left;
		temp.left= root;
		
		updateHeight(root);
		updateHeight(temp);
		
		return temp;
	}
	
	/**
	 * Makes a left-left rotation on the root node
	 * @param root: node to rotate
	 * @return rotated node
	 */
	private BSTNode rotateWithLeft(BSTNode root) {
		BSTNode temp = root.left;
		root.left = temp.right;
		temp.right = root;
		
		updateHeight(root);
		updateHeight(temp);
		
		return temp;
	}
	
	/**
	 * Make rotation when grand-child is inserted on left subtree
	 * of right subtree of root
	 * @param root: node to rotate
	 * @return rotated node
	 */
	private BSTNode doubleRotateWithRight(BSTNode root) {
		root.right = rotateWithLeft(root.right);
		root = rotateWithRight(root);
		return root;
	}
	
	/**
	 * Make rotation when grand-child is inserted on right subtree
	 * of left subtree of root
	 * @param root: node to rotate
	 * @return rotated node
	 */
	private BSTNode doubleRotateWithLeft(BSTNode root) {
		root.left = rotateWithRight(root.left);
		return rotateWithLeft(root);
	}
	
	/**
	 * Updates height of current node
	 * @param node: BST node to update
	 */
	@SuppressWarnings("unchecked")
	private void updateHeight(BSTNode node) {
		if (node == null) {
			return;
		}
		int leftHeight = node.left == null ? -1 : ((AVLNode)node.left).height;
		int rightHeight = node.right == null ? -1 : ((AVLNode)node.right).height;
		((AVLNode)node).height = Math.max(leftHeight, rightHeight) + 1;
	}
	
	/**
	 * Checks whether current node is balanced
	 * @param node: BST node to update
	 */
	@SuppressWarnings("unchecked")
	private boolean isBalanced(BSTNode node) {
		if (node == null) {
			return true;
		}
		int leftHeight = node.left == null ? -1 : ((AVLNode)node.left).height;
		int rightHeight = node.right == null ? -1 : ((AVLNode)node.right).height;
		return Math.abs(leftHeight - rightHeight) < 2;
	}
		
							
	/**
	 * Checks whether AVL tree is a valid, meaning
	 * it has binary-search property, ordering property,
	 * and all heights are correct
	 */
	@SuppressWarnings("unchecked")
	public boolean isValidTree() {
		return this.isValidTree((AVLNode)overallRoot);
	}
	
	/**
	 * Checks whether all nodes starting from root satisfy
	 * all properties of an AVL Node
	 */
	@SuppressWarnings("unchecked")
	private boolean isValidTree(AVLNode root) {
		if (root == null) {
			return true;
		} else {
			int leftHeight = root.left == null ? -1 : ((AVLNode)root.left).height;
			int rightHeight = root.right == null ? -1 : ((AVLNode)root.right).height;
			E leftVal = root.left.data;
			E rightVal = root.right.data;
			
			boolean leftValid = comparator.compare(leftVal, root.data) < 0;
			boolean rightValid = comparator.compare(root.data, rightVal) > 0;
			boolean heightValid = Math.abs(leftHeight - rightHeight) <= 1;
			boolean correctHeight = root.height == Math.max(leftHeight, rightHeight) + 1;
			
			return leftValid && rightValid && heightValid && correctHeight && isValidTree((AVLNode)root.left) 
					&& isValidTree((AVLNode)root.right);
		}
	}
	
	/** Private Class ==================================================**/

    /**
     * Inner class to represent a node in the tree. Each node includes
     * a data of type E, a height and an integer count. 
     */
    private class AVLNode extends BSTNode {
    	protected int height;
        /**
         * Create a new data node, does not increment tree's size
         * @param data data element to be stored at this node.
         */
        public AVLNode(E d) {
            super(d);
            // Super increments size. WE DON'T WANT THAT
            size--;
            this.height = 0;
        }
    }

}
